import csv
import paramiko
import time
NUM_STEPS=5
def display_progressbar():
    for i in range(NUM_STEPS):
        print('.' * (i+1), end='\r')
        time.sleep(0.6)

def wait_for_data(ssh_shell):
    while not ssh_shell.recv_ready():
        pass
    result = ssh_shell.recv(1024).decode('utf-8')
    return result
def wait_for_prompt(prompt:str,ssh_shell):
    print('waiting for command \n')
    result='None'
    while prompt not in result:
        result = wait_for_data(ssh_shell)
    pass
# Get the SSH username and host/IP address from the user
ssh_username = input('Enter the SSH username: ')
ssh_host = input('Enter the SSH host/IP address: ')
sudo_password = input('Enter the sudo password: ')

print('Make sure the correct files are on the files folder under the name of input_variables.csv')
display_progressbar()       

try:
    # Connect to the SSH session
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ssh_host, username=ssh_username, password=sudo_password)
    # Open a shell session
    ssh_shell = ssh.invoke_shell()
    wait_for_data(ssh_shell)
    # Send the sudo command and wait for the password prompt
    display_progressbar()
    print('entering as sudo user')
    ssh_shell.send('sudo su -\n')
    
    print('entering password for sudo user')
    wait_for_prompt('[sudo] password for',ssh_shell)
    
    ssh_shell.send(sudo_password + '\n')

    with open('input/input_variables.csv', newline='') as csvfile:
        print('reading csv_file')
        parameters = csv.reader(csvfile)
        next(parameters)
        for client_name, password in parameters:
           
            # Send the pivpn -a command
            print('entering pivpn -a command')
            ssh_shell.send('pivpn -a\n')
            wait_for_prompt('Enter a Name for the Client',ssh_shell)
    
            # Input the clientName variable from the CSV file
            print('creating user {} with password {}'.format(client_name, password))
            ssh_shell.send(client_name + '\n')
            wait_for_data(ssh_shell)
            
            # Wait for the command to prompt for the expire date (with default value)
            wait_for_prompt('How many days should the certificate last',ssh_shell)
            ssh_shell.send('\n')
            
            # Input the password variable from the CSV file
            wait_for_prompt('Enter the password for the client',ssh_shell)
            ssh_shell.send(password + '\n')

            # verifying password
            wait_for_prompt('Enter the password again to verify',ssh_shell)
            ssh_shell.send(password + '\n')
            
            #wait for creation
            wait_for_prompt('device and create additional profiles for other devices',ssh_shell)
            
        # Close the SSH session
        print('closing session')
        ssh.close()

except paramiko.AuthenticationException:
    print("Authentication failed, please verify your credentials.")
except paramiko.SSHException as sshException:
    print("Unable to establish SSH connection: %s" % sshException)
except paramiko.BadHostKeyException as badHostKeyException:
    print("Unable to verify server's host key: %s" % badHostKeyException)
except Exception as e:
    print('Exception occurred: %s' % e)

finally:

    # Always close the SSH session, if it was opened
    if ssh is not None:
        ssh.close()
print('Complete')

